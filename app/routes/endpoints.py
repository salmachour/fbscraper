from fastapi import APIRouter, Depends
from app.api.scraper import Scraper
from app.api.utils import ResponseModel,ErrorResponseModel
from fastapi.encoders import jsonable_encoder


router = APIRouter()


@router.get("/")
async def index():
    return {
        "info": "This is the index page of fastapi-fbscraper."
    }

@router.post("/collect/{page_name}/{post_limit}", response_description="Post data added into the database")
async def add_posts(page_name:str, post_limit:int, scraper: Scraper = Depends(Scraper)):
    post = scraper.collect(page_name,post_limit)
    post = jsonable_encoder(post)
    new_post = await scraper.insert_post(post)
    return ResponseModel(new_post, "All Posts Addes Successfully")

@router.get("/get", response_description="Post data retrieved from the database")
async def get_posts(scraper: Scraper = Depends(Scraper)):
    posts =  await scraper.get_all_posts()
    return ResponseModel(posts, "All Posts Returned Successfully")

@router.delete("/remove/{id}", response_description="Post data deleted from the database")
async def delete_post_data(id:str,scraper: Scraper = Depends(Scraper)):
    deleted_post =  scraper.delete_post(id)
    if deleted_post:
        return ResponseModel(
            "Posts with ID: {} removed".format(id), "Post deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "Post with id {0} doesn't exist".format(id)
    )
