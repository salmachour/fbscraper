from facebook_scraper import get_posts
import motor.motor_asyncio
from bson.objectid import ObjectId
import os


client = motor.motor_asyncio.AsyncIOMotorClient(os.environ["DATABASE_URL"])
database = client['DATABASE_NAME']
collection = database.get_collection("COLLECTION_NAME")


class Scraper():
    
    def __init__(self):
        pass
      
    def collect(self,page_name,post_limit):
        for post in get_posts(str(page_name), pages=int(post_limit)):
            return post

    async def insert_post(self,post_data):
        post = await collection.insert_one(post_data)
        new_post = await collection.find_one({'_id': 0})
        return new_post

    async def get_all_posts(self):
        posts = []
        async for post in collection.find():
            post['id'] = str(post['_id'])
            del[post['_id']]
            posts.append((post))
        return posts

    def delete_post(self, id):
        post = collection.find_one({"_id": ObjectId(id)})
        if post:
            collection.delete_one({"_id": ObjectId(id)})
            return True
